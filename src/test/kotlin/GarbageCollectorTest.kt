import net.bladehunt.reakt.reactivity.Effect
import net.bladehunt.reakt.reactivity.Memo
import net.bladehunt.reakt.reactivity.Signal
import org.junit.jupiter.api.Test
import java.lang.ref.WeakReference
import kotlin.test.assertNotNull
import kotlin.test.assertNull

/**
 * This test might fail due to JVM differences.
 */
object GarbageCollectorTest {
    @Test
    fun gcRemovesReferences() {
        var hardSignalRef: Signal<Int>? = Signal(5)
        val signalRef: WeakReference<Signal<Int>> = WeakReference(hardSignalRef)
        assertNotNull(signalRef.get())

        val memoRef = WeakReference(
            Memo {
                val test by signalRef.get()!!
                test * 15
            }
        )

        val effectRef = WeakReference(
            Effect {
                val value by memoRef.get()!!
                assert(value == 75 || value == 60)
            }
        )
        signalRef.get()?.also { signal ->
            var value by signal
            value = 4
        }

        assertNotNull(memoRef.get())
        assertNotNull(effectRef.get())
        assertNotNull(signalRef.get())

        hardSignalRef = null
        System.gc()

        assertNull(memoRef.get())
        assertNull(effectRef.get())
        assertNull(signalRef.get())
    }
}