import kotlinx.coroutines.delay
import kotlinx.coroutines.test.runTest
import net.bladehunt.reakt.reactivity.Effect
import net.bladehunt.reakt.reactivity.Resource
import kotlin.test.Test
import kotlin.test.assertEquals

object ResourceTest {
    @Test
    fun resourceTest(): Unit = runTest {
        val resource = Resource(scope = this) {
            delay(2000) // Delays are ignored in runTest
            "123"
        }
        var timesTriggered = 0
        Effect {
            assertEquals(resource(), if (timesTriggered == 0) null else "123")
            timesTriggered++
        }
    }
}