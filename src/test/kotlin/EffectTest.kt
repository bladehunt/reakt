import net.bladehunt.reakt.reactivity.Effect
import net.bladehunt.reakt.reactivity.Signal
import org.junit.jupiter.api.Test
import kotlin.test.assertContentEquals
import kotlin.test.assertEquals

object EffectTest {

    @Test
    fun testCollectedValues() {
        val collectedValues: MutableList<Int> = arrayListOf()
        val expectedValues: List<Int> = listOf(0,1,2,3,4,5)
        val (value, setValue) = Signal(0)
        Effect {
            collectedValues.add(value())
        }
        setValue(1)
        setValue(2)
        setValue(3)
        setValue(4)
        setValue(5)

        assertContentEquals(collectedValues, expectedValues)
    }
}