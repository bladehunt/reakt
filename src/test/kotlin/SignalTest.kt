import net.bladehunt.reakt.reactivity.Signal
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

object SignalTest {
    @Test
    fun delegateTest() {
        var value by Signal(0)
        assertEquals(value, 0)
        value++
        assertEquals(value, 1)
        value++
        assertEquals(value, 2)
        value++
        assertEquals(value, 3)
        value++
        assertEquals(value, 4)
        value++
        assertEquals(value, 5)
    }
    @Test
    fun deconstructTest() {
        val (value, setValue) = Signal(0)
        assertEquals(value(), 0)
        setValue(1)
        assertEquals(value(), 1)
        setValue(2)
        assertEquals(value(), 2)
        setValue(3)
        assertEquals(value(), 3)
        setValue(4)
        assertEquals(value(), 4)
        setValue(5)
        assertEquals(value(), 5)
    }
}