import net.bladehunt.reakt.reactivity.Effect
import net.bladehunt.reakt.reactivity.Memo
import net.bladehunt.reakt.reactivity.Signal
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import java.lang.ref.WeakReference
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertNotNull
import kotlin.test.assertNull

object MemoTest {
    @Test
    fun effectSubscribesToBoundMemo() {
        val person = Signal("world")
        val memo = Memo {
            val value by person
            "Hello $value!"
        }
        Effect {
            val memoValue by memo
            assert(memoValue == "Hello world!" || memoValue == "Hello mom!")
        }
        var value by person
        value = "mom"
    }

    @Test
    fun memoResultBlocks() {
        val num = Signal(5)
        val memo = Memo { memoResult ->
            val value by num
            if (value == 4) memoResult.block = true
            value
        }
        Effect {
            val memoValue by memo
            assertNotEquals(memoValue, 4)
        }
        var value by num
        value = 4
        value = 3
    }
}