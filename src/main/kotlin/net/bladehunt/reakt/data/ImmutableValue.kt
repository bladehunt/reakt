package net.bladehunt.reakt.data

/**
 * Represents any object with a single immutable value.
 */
interface ImmutableValue<T> {
    val value: T
}