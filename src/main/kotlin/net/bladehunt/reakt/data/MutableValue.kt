package net.bladehunt.reakt.data

/**
 * Represents any object with a single mutable value.
 */
interface MutableValue<T> : ImmutableValue<T> {
    override var value: T
}