package net.bladehunt.reakt.pubsub

import net.bladehunt.reakt.pubsub.event.Event

/**
 * An EventPublisher is anything that holds subscribers
 * and publishes any events to its subscribers.
 */
interface EventPublisher {
    val subscribers: Collection<EventSubscriber>

    fun subscribe(subscriber: EventSubscriber)
    fun unsubscribe(subscriber: EventSubscriber)

    fun publish(event: Event)
}