package net.bladehunt.reakt.pubsub

import net.bladehunt.reakt.pubsub.event.Event

/**
 * Any object that subscribes to a publisher.
 *
 * EventSubscriber#onEvent usually reruns a function.
 */
interface EventSubscriber {
    fun onSubscribe(publisher: EventPublisher)

    fun onEvent(event: Event)

    fun onUnsubscribe(publisher: EventPublisher)
}