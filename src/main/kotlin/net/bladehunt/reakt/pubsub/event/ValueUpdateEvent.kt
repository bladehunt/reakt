package net.bladehunt.reakt.pubsub.event

import net.bladehunt.reakt.pubsub.EventPublisher

data class ValueUpdateEvent<T>(
    override val publisher: EventPublisher,
    val oldValue: T?,
    val newValue: T,
) : Event