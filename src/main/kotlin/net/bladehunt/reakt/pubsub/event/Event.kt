package net.bladehunt.reakt.pubsub.event

import net.bladehunt.reakt.pubsub.EventPublisher

sealed interface Event {
    val publisher: EventPublisher
}