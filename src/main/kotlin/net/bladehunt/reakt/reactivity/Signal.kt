package net.bladehunt.reakt.reactivity

import net.bladehunt.reakt.data.MutableValue
import net.bladehunt.reakt.pubsub.EventPublisher
import net.bladehunt.reakt.pubsub.EventSubscriber
import net.bladehunt.reakt.pubsub.event.Event
import net.bladehunt.reakt.pubsub.event.ValueUpdateEvent
import java.util.Collections
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KProperty

/**
 * Signals are values that can be subscribed to. When
 * invoked in a ReactiveContext, they automatically
 * subscribe to the Signal.
 *
 * Ways to use Signals include the `by` keyword or invoking it.
 * ```
 * var signal by Signal(0)
 * Effect {
 *     println(signal) // 0, 1,...5
 * }
 * for (i in 1..5) {
 *     signal = i
 * }
 * ```
 * ```
 * val signal = Signal(0)
 * Effect {
 *     println(signal()) // 0, 1,...5
 * }
 * for (i in 1..5) {
 *     signal.value = i
 * }
 * ```
 *
 * @param default The default value of the signal.
 */
class Signal<T>(default: T) : EventPublisher, MutableValue<T> {
    override val subscribers: MutableSet<EventSubscriber> = Collections.newSetFromMap(ConcurrentHashMap())

    override var value: T = default
        set(value) {
            val old = field
            field = value
            publish(ValueUpdateEvent(this, old, field))
        }

    override fun subscribe(subscriber: EventSubscriber) {
        subscribers.add(subscriber)
        subscriber.onSubscribe(this)
    }

    override fun unsubscribe(subscriber: EventSubscriber) {
        subscribers.remove(subscriber)
    }
    
    override fun publish(event: Event) {
        subscribers.forEach {
            it.onEvent(event)
        }
    }

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T = value
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this@Signal.value = value
    }

    context (ReactiveContext)
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T = invoke()

    operator fun invoke(): T = value

    context (ReactiveContext)
    operator fun invoke(): T {
        if (subscribers.contains(this@ReactiveContext)) return value
        subscribe(this@ReactiveContext)
        return value
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Signal<*>

        if (value != other.value) return false
        if (subscribers != other.subscribers) return false

        return true
    }

    override fun hashCode(): Int {
        var result = value?.hashCode() ?: super.hashCode()
        subscribers.forEach {
            result = result * 31 + it.hashCode()
        }
        return result
    }

    inner class Left {
        operator fun invoke(): T = value

        context (ReactiveContext)
        operator fun invoke(): T {
            if (subscribers.contains(this@ReactiveContext)) return value
            subscribe(this@ReactiveContext)
            return value
        }
    }

    inner class Right {
        operator fun invoke(value: T) {
            this@Signal.value = value
        }
    }

    operator fun component1(): Left = Left()
    operator fun component2(): Right = Right()

    override fun toString(): String {
        return "Signal(value=$value, subscribers=$subscribers)"
    }
}