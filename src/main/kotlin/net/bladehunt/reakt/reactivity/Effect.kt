package net.bladehunt.reakt.reactivity

import net.bladehunt.reakt.pubsub.EventPublisher
import net.bladehunt.reakt.pubsub.event.Event

/**
 * Effects are subscribers that execute the function after
 * receiving events. Effects are run once when initialized
 * in order to subscribe to any publishers in the function.
 *
 * Usage
 * ```
 * var signal = Signal(0)
 * Effect {
 *     println(signal()) // Subscription happens here
 * } // This block will be re-run every time the signal gets updated
 * for (i in 1..5) {
 *     signal.value = i
 * }
 * ```
 *
 * @param block The function to execute
 */
class Effect(private val block: Effect.() -> Unit) : ReactiveContext {
    init {
        this.block()
    }

    override fun onSubscribe(publisher: EventPublisher) {}

    override fun onEvent(event: Event) = block()

    override fun onUnsubscribe(publisher: EventPublisher) {}
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Effect

        return block == other.block
    }

    override fun hashCode(): Int {
        return block.hashCode()
    }

    override fun toString(): String {
        return "Effect(block=$block)"
    }
}