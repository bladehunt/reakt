package net.bladehunt.reakt.reactivity

/**
 * MemoResult allows the user to block the result
 * from being passed on to the children.
 *
 * @param block Whether to block the event from being sent to the subscribers
 */
data class MemoResult(
    var block: Boolean = false
)