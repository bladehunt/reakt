package net.bladehunt.reakt.reactivity

import net.bladehunt.reakt.pubsub.EventPublisher
import net.bladehunt.reakt.pubsub.EventSubscriber
import net.bladehunt.reakt.pubsub.event.Event
import net.bladehunt.reakt.pubsub.event.ValueUpdateEvent
import java.util.Collections
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KProperty

/**
 * Memos are transformers that calculate data based on
 * subscribed values.
 *
 * @param block The function to execute
 */
class Memo<T>(private val block: Memo<T>.(memoResult: MemoResult) -> T) : EventPublisher, ReactiveContext {
    override val subscribers: MutableSet<EventSubscriber> = Collections.newSetFromMap(ConcurrentHashMap())

    init {
        this.block(MemoResult())
    }

    override fun subscribe(subscriber: EventSubscriber) {
        subscribers.add(subscriber)
        subscriber.onSubscribe(this)
    }

    override fun unsubscribe(subscriber: EventSubscriber) {
        subscribers.remove(subscriber)
    }
    
    override fun publish(event: Event) {
        subscribers.forEach {
            it.onEvent(event)
        }
    }

    operator fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return block(MemoResult())
    }

    context (ReactiveContext)
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T = invoke()

    operator fun invoke(): T {
        return block(MemoResult())
    }

    context (ReactiveContext)
    operator fun invoke(): T {
        val result = MemoResult()
        if (subscribers.contains(this@ReactiveContext)) return block(result)
        subscribe(this@ReactiveContext)
        return block(result)
    }

    override fun onSubscribe(publisher: EventPublisher) {}

    override fun onEvent(event: Event) {
        val result = MemoResult()
        val response = block(result)
        if (result.block) return
        publish(ValueUpdateEvent(this, null, response))
    }

    override fun onUnsubscribe(publisher: EventPublisher) {
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Memo<*>

        if (block != other.block) return false
        if (subscribers != other.subscribers) return false

        return true
    }

    override fun hashCode(): Int {
        var result = block.hashCode()
        subscribers.forEach {
            result = result * 31 + it.hashCode()
        }
        return result
    }

    override fun toString(): String {
        return "Memo(block=$block, subscribers=$subscribers)"
    }
}