package net.bladehunt.reakt.reactivity

import net.bladehunt.reakt.pubsub.EventSubscriber

/**
 * ReactiveContext represents any context, e.g. an
 * Effect, Memo, or any context that automatically
 * subscribes to any publishers used in the context.
 */
interface ReactiveContext : EventSubscriber