package net.bladehunt.reakt.reactivity

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.bladehunt.reakt.data.ImmutableValue
import net.bladehunt.reakt.pubsub.EventPublisher
import net.bladehunt.reakt.pubsub.EventSubscriber
import net.bladehunt.reakt.pubsub.event.Event
import net.bladehunt.reakt.pubsub.event.ValueUpdateEvent
import java.util.*
import java.util.concurrent.ConcurrentHashMap
import kotlin.reflect.KProperty

class Resource<T>(
    defaultValue: T,
    private val scope: CoroutineScope = CoroutineScope(Dispatchers.Default),
    private val block: suspend () -> T,
) : EventPublisher, ImmutableValue<T> {
    companion object {
        inline operator fun <T> invoke(
            scope: CoroutineScope = CoroutineScope(Dispatchers.Default),
            noinline block: suspend () -> T
        ) = Resource(null, scope, block)
    }

    init {
        scope.launch {
           value = block()
        }
    }

    fun refetch() {
        scope.launch {
            value = block()
        }
    }

    override val subscribers: MutableSet<EventSubscriber> = Collections.newSetFromMap(ConcurrentHashMap())

    override var value: T = defaultValue
        set(value) {
            val old = field
            field = value
            publish(ValueUpdateEvent(this, old, field))
        }

    override fun subscribe(subscriber: EventSubscriber) {
        subscribers.add(subscriber)
        subscriber.onSubscribe(this)
    }

    override fun unsubscribe(subscriber: EventSubscriber) {
        subscribers.remove(subscriber)
    }

    override fun publish(event: Event) {
        subscribers.forEach {
            it.onEvent(event)
        }
    }
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T = value
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this@Resource.value = value
    }

    context (ReactiveContext)
    operator fun getValue(thisRef: Any?, property: KProperty<*>): T = invoke()

    operator fun invoke(): T = value

    context (ReactiveContext)
    operator fun invoke(): T {
        if (subscribers.contains(this@ReactiveContext)) return value
        subscribe(this@ReactiveContext)
        return value
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Resource<*>

        if (scope != other.scope) return false
        if (block != other.block) return false
        if (subscribers != other.subscribers) return false
        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        var result = scope.hashCode()
        result = 31 * result + block.hashCode()
        result = 31 * result + subscribers.hashCode()
        result = 31 * result + (value?.hashCode() ?: 0)
        return result
    }
}