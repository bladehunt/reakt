# ReaKt
### A simple reactive Kotlin library

The goal of ReaKt was to make a small and fast reactivity library that feels similar to SolidJS.

ReaKt will *probably* have breaking API changes in the future and the is currently not tested for thread safety.

## Features
- [x] Reactive Providers
  - [x] Signals
    - Similar to SolidJS signals, but the value can be obtained and set using the *by* keyword
    - Example: <br>
        ```
      val signal = Signal(0)
      
      // If not used in a ReactiveContext, the code will not be re-run on change.
      val signalValue by signal
      println(signalValue) // 0
      signalValue = 1
      println(signalValue) // 1
      
      // Alternatively, you can do
      println(signal.value) // 1
      signal.value = 2
      println(signal.value) // 2
        ```
- [x] ReactiveContext
  - [x] Effect (ReactiveContext)
      - Example: <br>
          ```
        val signal = Signal(0)
        Effect {
            val signalValue by signal
            println(signalValue) // Should output 0 and 1
        }
        signalValue = 1
          ```
  - [x] Automatic subscriptions using either the *by* keyword or invoking
  - [x] Contexts get GC'd properly once all the signals
- [x] Memos
    - A mix of a publisher and subscriber
    - Can use signals/any other subscription
    - Function block gets called every time
    - Example: <br>
        ```
      val signal = Signal(0)
      val memo = Memo { memoResult ->
          // memoResult.block = true
          // The above line will block the event from reaching the memo's children
      
          // The compiler will infer that you want to return a number as well
          signal() * 5 // One liner (invoke can be used instead of by - setting will not work)
      }
      Effect {
          val signalValue by memo
          println(signalValue) // Should output 0 and 5
      }
      signalValue = 1
        ```