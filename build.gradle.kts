plugins {
    kotlin("jvm") version "1.9.22"
    `maven-publish`
}

group = "net.bladehunt"
version = "0.1.1"

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
            artifactId = "reakt"
        }
    }
    repositories {
        maven {
            url = uri("https://gitlab.com/api/v4/projects/54686082/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = properties["CI_JOB_TOKEN"] as String?
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

repositories {
    mavenCentral()
}

dependencies {
    compileOnly("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.8.0")
    testImplementation(kotlin("test"))
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.8.0")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(8)
    compilerOptions {
        freeCompilerArgs.add("-Xcontext-receivers")
    }
}